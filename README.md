 OWIN ( The open web interface for .NET) is an open source application describing an abstraction layer between web servers 
 and application components . The goal of the OWIN specification is to faciliate a simple pluggable architecture for .NET 
 based web applications and the servers upon which they rely , encouraging development of small,focused application 
 components (known as "middlewares" in the OWIN pariance) which can be assembled into a processing pipeline through which
  the server can then route incoming HTTP requests.
    OWIN is a specification , not an implementation . As such , OWIN describes a minimal set of types , and a single 
    application delegate thorugh which interactions b/w an application and server occurs.
  
  OWIN Definations :
  
   Server – The HTTP server that directly communicates with the client and then uses OWIN semantics to process requests. 
            Servers may require an adapter layer that converts to OWIN semantics.

Web Framework – A self-contained component on top of OWIN exposing its own object model or API that applications may use to
                facilitate request processing.  Web Frameworks may require an adapter layer that converts from OWIN semantics.

Web Application – A specific application, possibly built on top of a Web Framework, which is run using OWIN compatible Servers.

Middleware – Pass through components that form a pipeline between a server and application to inspect, route, or modify request 
        and response messages for a specific purpose.

Host – The process an application and server execute inside of, primarily responsible for application startup. Some Servers 
       are also Hosts. 
  
  The OWIN application delegate function :
  
   An application Delegate function will take the Enviroment Dictionary ( IDictionary<string,object>) as an argument, use the
   request/response information contained in the enviroment dictionary to perform whatever processing is required to meet its
   reponsibilities, and return a Task when processing is complete.
   
   The application Delegate Function Signature specified by OWIN :
   Func<IDictionary<string,object>,Task>
   
   In our code, we will often use the alias AppFunc for this delegate to improve readability:
    Alias the Application Delegate for use in code:
    using AppFunc = Func<IDictionary<string, object>, Task>;
  
   
   The OWIN Middleware Pipeline .
   ---------------------------------
    In keeping with the above, individual application components (middleware) perform their 
    processing duties when the AppFunc delgate is called. However, in order to maintain the 
    pipeline, each middleware component is also responsible for invoking the next component 
    in the chain (or, intentionally NOT calling the next component and short-circuiting the 
    chain if appropriate.
   
    http://typecastexception.com/image.axd?picture=owin-middleware-chain.png
    
    In light of this, each middleware component needs to provide an AppFunc delegate to be 
    called in order to do its own work in the pipeline, and also needs to receive a 
    reference to the next AppFunc delegate, to be called (in most cases) once the current 
    component has completed processing.
  
    In other words, a middleware can be expressed with a signature which accepts an 
    AppFunc delegate as an argument (which is retained and called as the next process 
    in the pipeline), and which returns an AppFunc Delegate (which is used to perform 
    the current middleware processing:
  
    Middleware Delegate Signature:
    Func<AppFunc, AppFunc>
  
    In code, this might look something like this:
    
    public AppFunc SomeMiddleware(AppFunc next)
   { 
    // Create and AppFunc using a Lambda expression:
    AppFunc appFunc = async (IDictionary<string, object> environment) =>
    {
        // Do some processing ("inbound")...
        // Invoke and await the next middleware component:
        await next.Invoke(environment);
 
        // Do additional processing ("outbound")
    };
    return appFunc;
    }
  
  What is Katana ?
   
    Katana is a set of open source components for building and hosting OWIN-based web applications,
    maintained by the Microsoft Open Technologies Group.
     
    Katana provides an implementation of the OWIN specification, and is in fact used in an 
    increasing number of Asp.Net project templates. Additionally , Katana provides a wide variety 
    of ready-to-use middleware components,ready for use in an OWIN-based application.
    
    For our purposes , we will use some basic components from katana to demonstrate and understand:
    
    1. How an OWIN-based middleware pipeline is configured
    2. How to construct a basic middleware component
    3. How OWIN and the middleware pipeline fit into a web application generally
    
    How all this comes together into the middleware pipeline, and the manner in which
    your application configures and interacts with it can be confusing at first.
    For one thing, we are dealing with a lot of delegate functions and generic types.
    Also, there are still some things happening behind the scenes that are not obvious at first.,

    The best way to understand how OWIN, Katana, and the middleware pipeline works is, well, to 
    jump in and mess about.
    
    
    Create a console application :
    -------------------------------
    1. Install package Microsoft.Owin.Hosting
    2. Install package Microsoft.Owin.Host.HttpListener  (for our application to listen to HTTP requests):
     
    we examine in this post will be overly (some might say "stupidly") simple, and let's bear in mind that
     we are focusing more on how basic middleware are constructed, and how the middleware pipeline works 
     in general than we are on how to write specific middleware components, or how to use all of the 
     Katana features. I stick with silly examples here so that we are focused on the core middleware 
     structure, and how the pipeline works, and not on complex middleware implementation details.
     
     KATANA and IAppBuilder interface :
     
     The IAppBuilder interface is not a part of the OWIN specification.It is however, a required component 
     for a Katana host The IAppBuilder interface provides a core set of methods required to implement the 
     OWIN standard, and serves as a base for additional extension methods for implementing middleware.
    
     When the Katana host initializes the Startup class and calls Configuration(), a concrete instance of
     IAppBuilder is passed as the argument. We then use IAppBuilder to configure and add the application 
     middleware components we need for our application, assembling the pipeline through which incoming 
     HTTP requests will be processed.

      The most common way to add middleware is by passing components to the Use() method. Middleware 
      components will be added to the pipeline in the order they are passed to Use(). This is important
       to bear in mind as we configure our pipeline, as this will determine the order in which processing 
       is applied to incoming requests (and in reverse, to outgoing responses).

      In our code, we grab a reference to our middleware function by calling MyMiddleware(), and then 
      add it to the pipeline be passing it to app.Use(). 
      
      Using Katana Abstractions : IOwinContext
             In our previous example, we worked with the raw Environment Dictionary as specified by OWIN.
     This provides a flexible, low-level mechanism, but is less than handy when we want to work with 
     strongly-typed objects, and perhaps raise our abstractions up a level in our pipeline implementation code.
     
     
     OWIN start from aspnet overview:
       The resulting abstraction consists of two core elements. The first is the environment dictionary. 
       This data structure is responsible for storing all of the state necessary for processing an HTTP request 
       and response, as well as any relevant server state. The environment dictionary is defined as follows:

        IDictionary<string, object>
        
        An OWIN compatible Web server is responsible for populating the enviorment dictionary with data such as
         the body streams and header collections for an HTTP request and response. It is then the responsibility of
         the application or framework components to populate or update the dictionary with additional values 
         and write to the response body stream.
         
        In addition to specifying the type for the environment dictionary, the OWIN specification defines a list of 
        core dictionary key value pairs. For example, the following table shows the required dictionary keys for 
        an HTTP request:  
        
       1."owin.RequestBody"  A stream with the request body, if any.
       2. "owin.RequestHeaders" An IDictionary<string, string[]> of request headers.
       3. "owin.RequestMethod" A string containing the HTTP request method of the request (e.g., "GET", "POST").
       4. "owin.RequestPath" A string containing the request path. The path MUST be relative to the "root" of 
                                the application delegate;
       5. "owin.RequestPathBase" A string containing the portion of the request path corresponding to the "root" 
                                of the application delegate;                     
       6. "owin.RequestProtocol" A string containing the protocol name and version (e.g. "HTTP/1.0" or "HTTP/1.1"). 
       7. "owin.RequestQueryString" A string containing the query string component of the HTTP request URI, without 
                                    the leading “?” (e.g., "foo=bar&baz=quux"). The value may be an empty string. 
                                    
       8. "owin.RequestScheme" A string containing the URI scheme used for the request (e.g., "http", "https");                                      
      