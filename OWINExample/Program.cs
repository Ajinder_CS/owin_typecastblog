﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;
using Owin;
using System.IO;
using Microsoft.Owin;

namespace OWINExample
{
    // use an aliase for the OWIN AppFunc: 
    // So we can refer to Func<IDictionary<string, object>, Task> by the name AppFunc.
    using AppFunc = Func<IDictionary<string, object>, Task>;
    class Program
    {
        static void Main(string[] args)
        {
            //WebApp is in Microsoft.Owin.Hosting
            WebApp.Start<StartUp>("http://localhost:8080");
            Console.WriteLine("Server started;Press enter to quit");
            Console.ReadLine();
        }
    }
    public class StartUp {
        //IAppBuilder is in OWIN
        public void Configuration(IAppBuilder app)
        {
            // Set up the configuration options:
            var options = new MyMiddlewareConfigOptions("Greetings!", "John");
            options.IncludeDate = true;

            // Pass options along in call to extension method:
            app.UseMyMiddlewareConfig(options);


        //    var middleware = new Func<AppFunc, AppFunc>(MyMiddleWare);
            //var otherMiddleWare = new Func<AppFunc, AppFunc>(MyOtherMiddleWare);
            //var stronglyTypedMiddleWare = new Func<AppFunc, AppFunc>(StronglyTypedMiddleWare);

           //Using Extensions 

           // app.UseMyMiddleware();
           // app.UseMyOtherMiddleware();
 
           // app.UseMyMiddleComponent("This is sample greeting Text");

             //Using Classes
            
           // app.Use<MyMiddleWareComponent>();
           // app.Use<MyOtherMiddleWareComponent>();


           // app.Use(stronglyTypedMiddleWare);
           // app.Use(otherMiddleWare);
         //   app.Use(middleware);
        }

        // Added a method in startUp class "MyMiddleWare" ,Which accepts an argument of type AppFunc named next and returns and AppFunc.
        // If we look closely, we see that the anonymous function returned by the MyMiddleWare() method ,
        // When invoked by the host against an incoming HTTP request,will perform some basic processing against the incoming request (actually writing the response body)
        // and will then invoke the AppFunc next passed in as an argument,passing to it the enviroment dictionary , and thereby continuing the pipeline processing of the request.

        // Bear in mind that MyMiddleWare() method simply returns the anonymous function to the caller, But doesn't invoke it. 
        // The Function will be added to the processing pipeline , and will be invoked when an incming HTTP request  needs to be processed.
        public AppFunc MyMiddleWare(AppFunc next)
        {
            // anonymous function 
            AppFunc appFunc = async (IDictionary<string, object> enviroment) =>
            {
                //Do something with the incoming request:
                var response = enviroment["owin.ResponseBody"] as Stream;
                using (var writer = new StreamWriter(response))
                {
                    await writer.WriteAsync("<h1>Hello from my first middleware </h1>");
                }
                //await next.Invoke(enviroment);
            };
            return appFunc;

        }

        public AppFunc MyOtherMiddleWare(AppFunc next)
        {
            // anonymous function 
            AppFunc appFunc = async (IDictionary<string, object> enviroment) =>
            {
                //Do something with the incoming request:
                var response = enviroment["owin.ResponseBody"] as Stream;
                using (var writer = new StreamWriter(response))
                {
                    await writer.WriteAsync("<h1>Hello from my second middleware </h1>");
                }
                await next.Invoke(enviroment);
            };
            return appFunc;

        }

        public AppFunc StronglyTypedMiddleWare(AppFunc next)
        {
            // anonymous function 
            AppFunc appFunc = async (IDictionary<string, object> enviroment) =>
            {
                IOwinContext context = new OwinContext(enviroment);
                await context.Response.WriteAsync("<h1> Hello from strongly typed middleware </h1>");
                await next.Invoke(enviroment);
            };
            return appFunc;

        }

    }

    public class MyMiddleWareComponent {

        AppFunc _next;
        public MyMiddleWareComponent(AppFunc next)
        {
            _next = next;
        }

        public async Task Invoke(IDictionary<string, object> enviroment)
        {
            IOwinContext context = new OwinContext(enviroment);
            await context.Response.WriteAsync("<h1>Hello from My First Middleware  Component</h1>");
            await _next.Invoke(enviroment);
        }
    
    }
    public class MyOtherMiddleWareComponent
    {

        AppFunc _next;
        public MyOtherMiddleWareComponent(AppFunc next)
        {
            _next = next;
        }

        public async Task Invoke(IDictionary<string, object> enviroment)
        {
            IOwinContext context = new OwinContext(enviroment);
            await context.Response.WriteAsync("<h1>Hello from My Second Middleware Component </h1>");
            await _next.Invoke(enviroment);
        }

    }

    public static class AppBuilderExtensions
    {
        public static void UseMyMiddleware(this IAppBuilder app)
        {
            app.Use<MyMiddleWareComponent>();
        }

        public static void UseMyOtherMiddleware(this IAppBuilder app)
        {
            app.Use<MyOtherMiddleWareComponent>();
        }

        //public static void UseMyMiddleComponent(this IAppBuilder app, string greeting)
        //{
        //    app.Use<MyMiddleComponent>(greeting);
        //}

       public static void UseMyMiddlewareConfig(this IAppBuilder app,
       MyMiddlewareConfigOptions configOptions)
        {
            app.Use<MyMiddleComponent>(configOptions);
        }

    }

    //Adding Middleware Configuration option.

    public class MyMiddleComponent
    { 
     AppFunc _next;
        //Add a member to hold the greeting.
     string _greeting;
        public MyMiddleComponent(AppFunc next,string greeting)
        {
            _next = next;
            _greeting = greeting;
        }

        public async Task Invoke(IDictionary<string, object> enviroment)
        {
            IOwinContext context = new OwinContext(enviroment);
            //Insert the _greeting into the display text.
            await context.Response.WriteAsync(string.Format("<h1>{0}</h1>",_greeting));
            await _next.Invoke(enviroment);
        }
    
    }

    public class MyMiddlewareConfigOptions
    {
        string _greetingTextFormat = "{0} from {1}{2}";
        public MyMiddlewareConfigOptions(string greeting, string greeter)
        {
            GreetingText = greeting;
            Greeter = greeter;
            Date = DateTime.Now;
        }

        public string GreetingText { get; set; }
        public string Greeter { get; set; }
        public DateTime Date { get; set; }

        public bool IncludeDate { get; set; }

        public string GetGreeting()
        {
            string DateText = "";
            if (IncludeDate)
            {
                DateText = string.Format(" on {0}", Date.ToShortDateString());
            }
            return string.Format(_greetingTextFormat, GreetingText, Greeter, DateText);
        }
    }

}
